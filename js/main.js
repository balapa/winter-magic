// $(document).ready(function(){
//   // Change this to the correct selector.
//   $('.logo-fixed').midnight();
// });

// var subscribeWrapper = document.querySelector(".subscribe-wrapper");
// console.log("the form should work");
// subscribeWrapper.setAttribute("action", "https://icreon.us7.list-manage.com/subscribe/post?u=d2bafd8854b08f4b7483ad01c&amp;id=af0d7979fa");

var rellax = new Rellax('.rellax');
var logoFixed = document.querySelector(".logo-fixed");

var scrollTos = document.querySelectorAll(".to");

scrollTos.forEach(function(to){

	to.addEventListener("click", function(e){

		e.preventDefault();

		TweenLite.to(window, 1, {
			scrollTo: {
				y: this.getAttribute("href"),
				offsetY: 150
			},
			ease: Power4.easeOut
		});

	});
});

var controller = new ScrollMagic.Controller({});

var charityTween = TweenMax.fromTo("#charity .parallax-el.one", 1,
	{ y: 150 },
	{ y: 0, ease: Linear.easeNone })

var charityScene = new ScrollMagic.Scene({
	triggerElement: "#charity .trigger.one",
	duration: 374
})
	.setTween(charityTween)
	// .addIndicators()
	.addTo(controller);

var charity2Tween = TweenMax.fromTo("#charity .parallax-el.two", 1,
	{ y: 150 },
	{ y: 0, ease: Linear.easeNone })

var charity2Scene = new ScrollMagic.Scene({
	triggerElement: "#charity .trigger.two",
	duration: 374
})
	.setTween(charity2Tween)
	// .addIndicators()
	.addTo(controller);

// appreciation
var appreTween = TweenMax.fromTo("#appreciation .parallax-el", 1,
	{ y: 150 },
	{ y: 0, ease: Linear.easeNone })

var appreScene = new ScrollMagic.Scene({
	triggerElement: "#appreciation .trigger",
	duration: 306
})
	.setTween(appreTween)
	// .addIndicators()
	.addTo(controller);

// scroll events

var header = document.getElementById("header");
var headerHeight = header.clientHeight;
var winterLogo = document.querySelector(".winter-magic-logo");

var winterLogoWatcher = scrollMonitor.create(winterLogo, -headerHeight);

winterLogoWatcher.enterViewport(function() {
  console.log( 'I have entered the viewport' );
  header.classList.add("hide-logo");
});

winterLogoWatcher.exitViewport(function() {
  console.log( 'I have left the viewport' );
  header.classList.remove("hide-logo");
});

var flyIns = document.querySelectorAll(".fly-in");

flyIns.forEach(function(el){

	el.classList.add("hide-fly");
	var elWatcher = scrollMonitor.create(el, -headerHeight);

	elWatcher.fullyEnterViewport(function(){
		TweenMax.to(el, .3, {
			opacity: 1,
			y: 0
		})
	});

});

// align right image
var remainingMarginRight = (window.innerWidth - 960)/2;

var alignRights = document.querySelectorAll(".align-right");
alignRights.forEach(function(el){

	var extendedSize = el.clientWidth - 440;
	el.style.transform = "translateX(" + (remainingMarginRight - extendedSize) + "px)";
});


// popup
var popupRSVP = document.querySelector(".popup.rsvp");
var openPopupRSVP = document.querySelectorAll(".open-popup-rsvp");
var closePopupRSVP = document.querySelectorAll(".close-popup-rsvp, .close-popup-decline-rsvp");

var popupDecline = document.querySelector(".popup.decline-rsvp");
var openDecline = document.querySelector(".open-popup-decline");

openPopupRSVP.forEach(function(open){

	open.onclick = function(e){
		e.preventDefault();
		popupRSVP.classList.add("show");
	}
});

openDecline.onclick = function(e){
	e.preventDefault();
	popupDecline.classList.add("show");
}

closePopupRSVP.forEach(function (close) {
	close.onclick = function () {
		closeAllPopups();
	};
});

var body = document.body;
body.onkeyup = function (e) {
	if (e.keyCode === 27) {
		closeAllPopups();
	}
}

function closeAllPopups(){
	popupRSVP.classList.remove("show");
	popupDecline.classList.remove("show");
}


// decline rsvp
var nextScenes = document.querySelectorAll(".next-scene");
var scenes = document.querySelectorAll(".scene");
var scenesWrapper = document.querySelector(".scenes-wrapper");
var currentScene = scenesWrapper.querySelector(".show");
var nextScene;

nextScenes.forEach(function(next){

	next.onclick = function(){
		
		currentScene.style.transform = "translateY(-30px)";
		currentScene.classList.remove("show");

		currentScene.nextElementSibling.style.transform = "none";
		currentScene.nextElementSibling.classList.add("show");

		currentScene = currentScene.nextElementSibling;

	}

});


// mobile nav

var openMobileNav = document.querySelector(".open-mobile-nav");
var closeMobileNav = document.querySelector(".close-mobile-nav");

openMobileNav.onclick = function(){
	body.classList.add("show-mobile-nav");
}

closeMobileNav.onclick = function(){
	body.classList.remove("show-mobile-nav");
}

// get parameter name
function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var preEmail = getParameterByName('e');
var emailInputs = document.querySelectorAll(".email-input");

emailInputs.forEach(function(input){
	input.value = preEmail;
});

// regretfully confirm
var confirm = document.querySelector(".regretfully-confirm");

confirm.onclick = function(e){
	// e.preventDefault();
	// closeAllPopups();
	// scenes.forEach(function(scene){
	// 	scene.style.cssText = "";
	// });
	// currentScene.classList.remove("show");
	// currentScene = scenes[0];
	// currentScene.classList.add("show");
}